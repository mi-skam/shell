#!/bin/bash

#/ Usage: add <first number> <second number>
#/ Compute the sum of two numbers
usage() {
	grep '^#/' "$0" | cut -c4-
	exit 0
}

expr "$*" : ".*--help" > /dev/null && usage