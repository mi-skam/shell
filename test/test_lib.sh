source ../lib.sh

t_trim () {
    local foo="$1"
    trim foo
    assertEquals "$foo" "$2"
}

t_trim_f () {
    local foo="$1"
    trim foo
    assertNotEquals "$foo" "$2"
}

t_pause() {
	local answer
	answer=$(pause <<< "\n")
	assertEquals "$answer" "Press any key to continue..."
}

t_asksure() {
	local answer="$1"
	local assertion="$2"
	$(asksure <<< "$answer" &>/dev/null)
	assertEquals $? $?
}

test_trim() {
	t_trim bar bar
	t_trim '  bar' bar
	t_trim_f 'ho  ' bar
	t_trim 'bar bar' 'bar bar'
	t_trim '  bar  bar  ' 'bar  bar'
	t_trim $'\n\n\t foo\n\t bar \t\n' $'foo\n\t bar'
	t_trim $'\n' ''
	t_trim '\n' '\n'
}

test_pause() {
	t_pause
}

test_asksure() {
	t_asksure "y" 0
	t_asksure "n" 1
}
source shunit2