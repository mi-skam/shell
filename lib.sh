# bash only
trim () {
    read -r -d '' $1 <<< ${!1}
}

pause() {
  local dummy
  echo "Press any key to continue..."
  read -s -r dummy
}

asksure() {
	echo "Are you sure (y/n)? "
	while read -r answer; do
		[[ $answer = [Yy] ]]  && return 0
	    [[ $answer = [Nn] ]] && return 1
	done
}