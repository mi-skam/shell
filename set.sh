#!/bin/bash

# unofficial strict mode
set -euo pipefail
IFS=$'\n\t'


# set -e -> exits if any command exits with non-zero return
command_return_non_zero(){
	return 1
}

command_return_non_zero || true

# temporarily disabling the option
set +e
command_return_non_zero
set -e

# set -u -> prevent using undefined variables

my_arg=${1:-"default"}
echo $my_arg

# set -o pipefail -> fails on first non-zero status

#IFS=$'\n\t' ->
# makes iterations and splitting less surprising,
# in the case of loops mostly. The default for this
# variable is usually IFS=$' \n\t' but the space as
# a separator often gives confusing results.

cleanup() {
	# will be executed at EXIT
	#
	# remove temporary files
	# restart services
	echo "cleaned."
}

trap cleanup EXIT